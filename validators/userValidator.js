const joi = require('@hapi/joi');
const responseManager = require('../utils/responseManager');

/**
 * @name : registerValidation
 * @description : to validation the req. body of user registration body 
 * @author : Parshav Shah
 */
exports.registerValidation = async (req, res, next) => {
    try {

        const registerBody = joi.object({
            name: joi.string().required(),
            email: joi.string().email().required(),
            password: joi.string().required(),
            confirmPassword: joi.string().required().valid(joi.ref('password')),
        })

        let Result = await registerBody.validateAsync(req.body).catch((error) => {
            res['data'] = error;
            res['message'] = "Validation Error";
            res['code'] = 500;
            throw error;
        })

        if (Result) {
            next();
        }

    } catch (error) {
        responseManager.sendResponse(res);
    }
}