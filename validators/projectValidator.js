const joi = require('@hapi/joi');
const responseManager = require('../utils/responseManager');

/**
 * @name : createUpdateValidation
 * @author : Parshav Shah
 */
exports.createUpdateValidation = async (req, res, next) => {
    try {

        const createBody = joi.object({
            title: joi.string().required(),
            slug: joi.string().required(),
            projectTypeId: joi.string().required(),
            tags: joi.string().required(),
            body: joi.string().required(),
            image: joi.string().optional(),
        })

        let Result = await createBody.validateAsync(req.body).catch((error) => {
            res['data'] = error;
            res['message'] = "Validation Error";
            res['code'] = 500;
            throw error;
        })

        if (Result) {
            next();
        }

    } catch (error) {
        console.log(error)
        responseManager.sendResponse(res);
    }
}