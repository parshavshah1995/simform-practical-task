'use strict';
var uniqid = require('uniqid');
module.exports = (sequelize, DataTypes) => {

    var ProjectType = sequelize.define('ProjectType', {
        id: {
            type: DataTypes.STRING(36),
            allowNull: false,
            primaryKey: true,
            defaultValue: () => {
                return uniqid();
            }
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'types',
        timestamps: true,
        paranoid: true,
    });

    ProjectType.associate = (n) => {
        //    
    };

    return ProjectType;
};