'use strict';
var uniqid = require('uniqid');
module.exports = (sequelize, DataTypes) => {

    var ProjectTag = sequelize.define('ProjectTag', {
        id: {
            type: DataTypes.STRING(36),
            allowNull: false,
            primaryKey: true,
            defaultValue: () => {
                return uniqid();
            }
        },
        projectId: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        tagId: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'projectTags',
        timestamps: true,
        paranoid: true,
    });

    

    return ProjectTag;
};