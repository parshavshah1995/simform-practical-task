'use strict';
var uniqid = require('uniqid');
module.exports = (sequelize, DataTypes) => {

    var ProjectUser = sequelize.define('ProjectUser', {
        id: {
            type: DataTypes.STRING(36),
            allowNull: false,
            primaryKey: true,
            defaultValue: () => {
                return uniqid();
            }
        },
        projectId: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        userId: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'projectUsers',
        timestamps: true,
        paranoid: true,
    });

    ProjectUser.associate = (m) => {
        m.User.belongsToMany(m.Project, {
            through: m.ProjectUser
        });
        m.Project.belongsToMany(m.User, {
            through: m.ProjectUser
        });
    };

    return ProjectUser;
};