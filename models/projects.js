'use strict';
var uniqid = require('uniqid');
module.exports = (sequelize, DataTypes) => {

    var Project = sequelize.define('Project', {
        id: {
            type: DataTypes.STRING(36),
            allowNull: false,
            primaryKey: true,
            defaultValue: () => {
                return uniqid();
            }
        },
        projectTypeId: {
            type: DataTypes.STRING(36),
            allowNull: false
        },
        title: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        slug: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        image: {
            type: DataTypes.STRING(200),
            allowNull: false
        },
        body: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'projects',
        timestamps: true,
        paranoid: true
    });

    Project.associate = (m) => {
        m.Project.belongsTo(m.ProjectType, {
            foreignKey: 'projectTypeId',
            required: false
        });
        m.ProjectType.hasMany(m.Project, {
            foreignKey: 'projectTypeId',
            required: false
        });
        m.Project.belongsToMany(m.Tag, {
            through: 'ProjectTag',
            foreignKey: 'projectId',
            otherKey: 'tagId'
        });
        m.Project.belongsToMany(m.User, {
            through: 'ProjectUser',
            foreignKey: 'projectId',
            otherKey: 'userId'
        });
    };


    return Project;
};