'use strict';
var PasswordHash = require('password-hash');
var uniqid = require('uniqid');
module.exports = (sequelize, DataTypes) => {

    var User = sequelize.define('User', {
        id: {
            type: DataTypes.STRING(36),
            allowNull: false,
            primaryKey: true,
            defaultValue: () => {
                return uniqid();
            }
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        email: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        password: {
            type: DataTypes.STRING(200),
            allowNull: false,
            set(val) {
                this.setDataValue('password', PasswordHash.generate(val));
            }
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'users',
        timestamps: true,
        paranoid: true,
    });


    User.associate = (m) => {
        m.User.belongsToMany(m.Project, {
            through: 'ProjectUser',
            foreignKey: 'userId',
            otherKey: 'projectId'
        });
    };

    return User;
};