'use strict';
var uuid = require('uuid');
module.exports = (sequelize, DataTypes) => {

    var Tag = sequelize.define('Tag', {
        id: {
            type: DataTypes.STRING(36),
            allowNull: false,
            primaryKey: true,
            defaultValue: () => {
                return uuid();
            }
        },
        name: {
            type: DataTypes.STRING(100),
            allowNull: false
        },
        createdAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        updatedAt: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        },
        deletedAt: {
            type: DataTypes.DATE,
            allowNull: true
        }
    }, {
        tableName: 'tags',
        timestamps: true,
        paranoid: true,
    });

    Tag.associate = (m) => {
        m.Tag.belongsToMany(m.Project, {
            through: 'ProjectTag',
            otherKey: 'projectId',
            foreignKey: 'tagId'
        });
    };

    return Tag;
};