const express = require('express');
const router = express.Router();

const commonFunctions = require('../utils/commonFunctions');
const upload = commonFunctions.upload;

const userController = require('../controllers/userController');
const projectController = require('../controllers/projectController');

const userValidator = require('../validators/userValidator');
const projectValidator = require('../validators/projectValidator');

// --- User APIs
router.post('/user/login', commonFunctions.parseNone, userController.login)
router.post('/user/register', commonFunctions.parseNone, userValidator.registerValidation, userController.register)

// --- Project APIs
router.post('/project/create', userController.authnticate, upload.single('image'), commonFunctions.toSetImageInBody, projectValidator.createUpdateValidation, projectController.createRecord)
router.get('/project/read/:id', userController.authnticate, projectController.readRecord)
router.patch('/project/update/:id', commonFunctions.parseNone, userController.authnticate, projectValidator.createUpdateValidation, projectController.updateRecord)
router.delete('/project/delete/:id', userController.authnticate, projectController.deleteRecord)
router.get('/project/list', userController.authnticate, projectController.listRecords)

module.exports = router;