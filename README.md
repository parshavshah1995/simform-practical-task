# Project Management ExpressJS App

---
#### /user/login
- to login the user, it will return the token and set it in Authorization header
#### /user/register
- to register the user
#### /project/create
- it need the the Authorization token for creating the project
#### /project/read
- will return the project details with all realted infomation
#### /project/update
- will update the project
#### /project/delete
- will delete the project
#### /project/list
- will list the project
#### /project/list?search=test
- will search in project from the title
#### /project/list?filterByType=Finance
- will filter by type name

---

I've added the postman JSON and Database SQL file with it.