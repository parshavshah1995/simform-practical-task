-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 11, 2019 at 07:42 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projectManagement`
--

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `id` varchar(36) NOT NULL,
  `projectTypeId` varchar(36) NOT NULL,
  `title` tinytext NOT NULL,
  `slug` tinytext NOT NULL,
  `image` tinytext NOT NULL,
  `body` text NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='to store the projects';

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `projectTypeId`, `title`, `slug`, `image`, `body`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
('8afebef3-11ce-4160-8f96-a1e99367d2a7', '2pa4phc12ik41gp128', 'test', 'test', 'public/uploads/image-1576089670741.png', '<body>parshav</body>', '2019-12-11 18:41:10', '2019-12-11 18:41:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projectTags`
--

CREATE TABLE `projectTags` (
  `id` varchar(36) NOT NULL,
  `projectId` varchar(36) NOT NULL,
  `tagId` varchar(36) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='to store the tags';

--
-- Dumping data for table `projectTags`
--

INSERT INTO `projectTags` (`id`, `projectId`, `tagId`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
('225fe15b-59ab-451b-9be9-2aac1b3f4cc9', '8afebef3-11ce-4160-8f96-a1e99367d2a7', '2pa4phc12ik41gp125', '2019-12-11 18:41:10', '2019-12-11 18:41:10', NULL),
('9ba09a7e-d39d-4f38-a8ed-f7b07b0dab8e', '8afebef3-11ce-4160-8f96-a1e99367d2a7', '2pa4phc12ik41gp124', '2019-12-11 18:41:10', '2019-12-11 18:41:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `projectUsers`
--

CREATE TABLE `projectUsers` (
  `id` varchar(36) NOT NULL,
  `projectId` varchar(36) NOT NULL,
  `userId` varchar(36) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='to store the project_users';

--
-- Dumping data for table `projectUsers`
--

INSERT INTO `projectUsers` (`id`, `projectId`, `userId`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
('35344fd0-f0b4-4885-9563-a140db1b9935', '8afebef3-11ce-4160-8f96-a1e99367d2a7', '2pa4phc12ik41gscvk', '2019-12-11 18:41:10', '2019-12-11 18:41:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='to store the tags';

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
('2pa4phc12ik41gp124', 'Testing', '2019-12-11 21:13:06', '2019-12-11 21:13:06', NULL),
('2pa4phc12ik41gp125', 'Development', '2019-12-11 21:13:06', '2019-12-11 21:13:06', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `types`
--

CREATE TABLE `types` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='to store the project_types';

--
-- Dumping data for table `types`
--

INSERT INTO `types` (`id`, `name`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
('2pa4phc12ik41gp128', 'E-Commerce', '2019-12-11 21:14:15', '2019-12-11 21:14:15', NULL),
('2pa4phc12ik41gp129', 'Finance', '2019-12-11 21:14:15', '2019-12-11 21:14:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` varchar(36) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deletedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='to store the users';

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `createdAt`, `updatedAt`, `deletedAt`) VALUES
('2pa4phc12ik41gscvk', 'parshav', 'parshav@gmail.com', 'sha1$cea12da3$1$45c15bfbe0e87cec8e35dae82dfd5cbe9438cfc1', 0, '2019-12-11 15:44:34', '2019-12-11 15:44:34', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projectTags`
--
ALTER TABLE `projectTags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `projectUsers`
--
ALTER TABLE `projectUsers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `types`
--
ALTER TABLE `types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
