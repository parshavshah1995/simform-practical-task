const models = require('../models/');

exports.getProjectWithTypesTagsUsers = async (id) => {
    return models.Project.findOne({
        where: {
            id: id
        },
        attributes: ['id', 'title', 'slug', 'image', 'body'],
        include: [{
                model: models.ProjectType,
                required: false,
                attributes: ['id', 'name']
            },
            {
                model: models.Tag,
                required: false,
                attributes: ['id', 'name'],
                through: {
                    model: models.ProjectTags,
                    attributes: [],
                    required: false
                }
            },
            {
                model: models.User,
                required: false,
                attributes: ['id', 'name'],
                through: {
                    model: models.ProjectUser,
                    attributes: [],
                    required: false
                }
            },
        ]
    }).catch((error) => {
        throw error;
    });
}