const path = require('path');
const multer = require('multer')

exports.parseNone = multer().none();

exports.toSetImageInBody = (req, res, next) => {
    if (req && req.file && req.file.path) {
        req.body['image'] = req.file.path;
    }
    next();
}

var storage = exports.storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, './public/uploads')
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
});

exports.upload = multer({
    storage: storage
});