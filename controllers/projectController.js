const jwt = require('jsonwebtoken');
const responseManager = require('../utils/responseManager');
const models = require('../models/');
const _ = require('lodash');
const pcq = require('../commonQuery/projectQuery');

/**
 * @name : createRecord
 * @author : Parshav Shah
 */
exports.createRecord = async (req, res) => {
    try {

        let preparedData = {
            ...req.body
        };

        // slug validation checking
        let SlugCount = await models.Project.count({
            where: {
                slug: {
                    [models.Sequelize.Op.like]: preparedData['slug']
                }
            }
        })
        if (SlugCount > 0) {
            throw new Error("Slug must be unique");
        }

        let t1 = await models.sequelize.transaction().catch((error) => {
            throw error;
        })

        // create project
        let project = await models.Project.create(preparedData, {
            transaction: t1
        }).catch((error) => {
            t1.rollback();
            throw error;
        });

        // add tags in project
        req.body['tags'] = JSON.parse(req.body['tags']);
        let Tags = [];
        if (req.body['tags'] && req.body['tags'].length) {
            req.body['tags'].forEach(tag => {
                Tags.push({
                    tagId: tag,
                    projectId: project['id']
                })
            });
        }
        await models.ProjectTag.bulkCreate(Tags, {
            transaction: t1
        }).catch((error) => {
            t1.rollback();
            throw error;
        });

        // add user in project
        await models.ProjectUser.create({
            projectId: project['id'],
            userId: req.user['id']
        }, {
            transaction: t1
        }).catch((error) => {
            t1.rollback();
            throw error;
        });

        t1.commit();

        res['code'] = 201;
        res['data'] = await pcq.getProjectWithTypesTagsUsers(project['id']).catch((error) => {
            throw error;
        });;
        res['message'] = "Project created";

        responseManager.sendResponse(res);

    } catch (error) {
        res['message'] = error.message;
        res['data'] = error;
        res['code'] = 500;
        responseManager.sendResponse(res);
    }
}

/**
 * @name : updateRecord
 * @author : Parshav Shah
 */
exports.updateRecord = async (req, res) => {
    try {

        let recordId = req.params.id;
        let preparedData = {
            ...req.body
        };

        // update project
        let project = await models.Project.update(preparedData, {
            where: {
                id: recordId
            }
        }).then(() => {
            return pcq.getProjectWithTypesTagsUsers(recordId).catch((error) => {
                throw error;
            });
        }).catch((error) => {
            throw error;
        });
        res['data'] = project;
        res['message'] = "Project updated";

        responseManager.sendResponse(res);

    } catch (error) {
        res['message'] = error.message;
        res['data'] = error;
        res['code'] = 500;
        responseManager.sendResponse(res);
    }
}

/**
 * @name : readRecord
 * @author : Parshav Shah
 */
exports.readRecord = async (req, res) => {
    try {

        let recordId = req.params.id;

        // read project
        let project = await pcq.getProjectWithTypesTagsUsers(recordId).catch((error) => {
            throw error;
        });

        if (project) {
            res['data'] = project;
            res['message'] = `project get success ${project['id']}`;
            responseManager.sendResponse(res);
        } else {
            throw new Error("Project not found")
        }

    } catch (error) {
        res['message'] = error.message;
        res['data'] = error;
        res['code'] = 500;
        responseManager.sendResponse(res);
    }
}

/**
 * @name : deleteRecord
 * @author : Parshav Shah
 */
exports.deleteRecord = async (req, res) => {
    try {

        let recordId = req.params.id;

        // delete project
        let project = await models.Project.destroy({
            where: {
                id: recordId
            }
        }).catch((error) => {
            throw error;
        });
        res['code'] = 200;
        res['message'] = "Project deleted";

        responseManager.sendResponse(res);

    } catch (error) {
        res['message'] = error.message;
        res['data'] = error;
        res['code'] = 500;
        responseManager.sendResponse(res);
    }
}

/**
 * @name : listRecords
 * @author : Parshav Shah
 */
exports.listRecords = async (req, res) => {
    try {

        let options = {};

        // if search exist
        if (req.query.search) {
            options = {
                where: {
                    title: {
                        [models.Sequelize.Op.like]: `%${req.query.search}%`
                    }
                }
            }
        }

        // if filter exist
        if (req.query.filterByType) {
            console.log(req.query.filterByType)
            options = {
                include: [{
                    model: models.ProjectType,
                    attributes: [],
                    where: {
                        name: {
                            [models.Sequelize.Op.like]: `%${req.query.filterByType}%`
                        }
                    }
                }]
            }
        }

        // read all projects
        let projects = await models.Project.findAll(options).catch((error) => {
            throw error;
        });

        if (projects) {
            res['data'] = projects;
            res['message'] = `Projects list get success`;
            responseManager.sendResponse(res);
        } else {
            throw new Error("Project not found")
        }

    } catch (error) {
        res['message'] = error.message;
        res['data'] = error;
        res['code'] = 500;
        responseManager.sendResponse(res);
    }
}