const jwt = require('jsonwebtoken');
const responseManager = require('../utils/responseManager');
const models = require('../models/');
const passwordHash = require('password-hash');
const key = require('../config/config').development.key;
const _ = require('lodash');

/**
 * @name : register
 * @description : to register the user
 * @author : Parshav Shah
 */
exports.register = async (req, res) => {
    try {
        let PreparedData = {
            ...req.body
        };

        let User = {};

        // check that user is exist or not
        User = await models.User.count({
            where: {
                email: PreparedData.email
            }
        }).catch((error) => {
            throw error;
        })

        // User not exist so create the user
        if (!User) {
            User = await models.User.create(PreparedData).catch((error) => {
                throw error;
            });
            res['data'] = User;
            res['message'] = "User created";
        }
        // User exist so throw the error
        else {
            throw new Error("User Exist");
        }

        // Send the response
        responseManager.sendResponse(res);

    } catch (error) {
        res['message'] = error.message;
        res['data'] = error;
        res['code'] = 500;
        responseManager.sendResponse(res);
    }
}

/**
 * @name : login
 * @description : to register the user
 * @author : Parshav Shah
 */
exports.login = async (req, res) => {
    try {

        let PreparedData = {
            ...req.body
        };

        // check that user is exist or not
        let User = await models.User.findOne({
            where: {
                email: PreparedData.email
            },
            attributes: ['id', 'email', 'password'],
            raw: true
        }).catch((error) => {
            throw error;
        })

        // User not exist so create the user
        if (User) {

            // verify password
            let VerifyResult = passwordHash.verify(PreparedData.password, User.password);

            if (VerifyResult) {

                User = _.omit(User, ['password'])

                let Token = jwt.sign(User, key, {
                    expiresIn: '10h'
                });

                User['token'] = Token;

                res.data = User;
                res.message = "Login Success";
                res.code = 200;

                // Send the response
                responseManager.sendResponse(res);
            } else {
                throw new Error("Wrong password inserted");
            }
        }
        // User exist so throw the error
        else {
            throw new Error("User not exist");
        }

    } catch (error) {
        res.message = error.message;
        res.data = error;
        responseManager.sendResponse(res);
    }
}

exports.authnticate = (req, res, next) => {
    try {
        let CurruntTime = +new Date();
        jwt.verify(req.headers.authorization, key, (error, Decoded) => {
            if (error) {
                throw error;
            }
            if (Decoded.exp <= CurruntTime) {
                req['user'] = Decoded;
                // res['message'] = "Success";
                // res['data'] = true;
                // res['code'] = 200;
                // responseManager.sendResponse(res);
                next();
            } else {
                throw new Error("Something went wrong, Please login again.");
            }
        });
    } catch (error) {
        res['message'] = error.message;
        res['data'] = error;
        res['code'] = 401;
        responseManager.sendResponse(res);
    }
}